#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 13:15:17 2017

@author: devantormey
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 15:03:58 2017

@author: devantormey

"""
import math
#newline = None

def print_adcs(json_list,adcs_print):  
   if adcs_print[0] == 1:   
       if json_list[2]['agent_proc'] == 'adcs':
           #basic info
           if adcs_print[1] == 1:       
               if 'agent_utc' in json_list[0].keys():
                       print('\n Agent Type: ',json_list[2]['agent_proc'])
                       print('agent_utc (time): ', json_list[0]['agent_utc'])        
                       print('agent_node: ', json_list[1]['agent_node'])
                       print('cpu: ', json_list[6]['agent_cpu'], ' Memory: ', \
                             json_list[7]['agent_memory'], ' Jitter: ', \
                             json_list[8]['agent_jitter'], ' UTC Offset: ', json_list[9]['node_utcoffset'], )
        #Battery Print 
           if adcs_print[2] == 1:        
                for item in json_list:
                    if 'node_powgen' in item.keys():
                        print('Power Gen: ',item['node_powgen'],',',end = "")
                        newline = newline + str(item['node_powgen'])
                                    
                    if 'node_powuse' in item.keys():
                        print('Power Use: ',item['node_powuse'],',',end = "")
                                    
                    if 'node_charging' in item.keys():
                        print('Charging: ',item['node_charging'],',',end = "")
                                    
                    if 'node_battlev' in item.keys():
                        print('Batt Level: ',item['node_battlev'],',',end = "")
           #location below earth
           if adcs_print[3] == 1:
               for item in json_list:
                   if 'node_loc_bearth' in item.keys():
                       print('Location Bearth: ',item['node_loc_bearth'],',',end = "")
    
           #node Loc Pos
           if adcs_print[4] == 1:
               for item in json_list:
                   if 'node_loc_pos_eci' in item.keys():
                       print('Loc Pos ECI: ', ' Pos: ' ,item['node_loc_pos_eci']['pos'], \
                             ' Velocity: ' ,item['node_loc_pos_eci']['vel'], \
                             ' Acceleration: ' ,item['node_loc_pos_eci']['acc'],',',end = "")
                       
           if adcs_print[5] == 1:
               for item in json_list:           
                   if 'node_loc_att_icrf' in item.keys():
                       print('Loc Att ICRF: ', ' Pos: ' ,'[',item['node_loc_att_icrf']['pos']['d']['x'], \
                              item['node_loc_att_icrf']['pos']['d']['y'], \
                              item['node_loc_att_icrf']['pos']['d']['z'], \
                              item['node_loc_att_icrf']['pos']['w'],']' , \
                              ' Velocity: ' ,item['node_loc_att_icrf']['vel'], \
                              ' Acceleration: ' ,item['node_loc_att_icrf']['acc'],',',end = "")
                       
           if adcs_print[6] == 1:
               for item in json_list:           
                    if 'device_ssen_utc_000' in item.keys():
                         print('Device SSen UTC: ',item['device_ssen_utc_000'],',',end = "") 
                         
           if adcs_print[7] == 1:
               for item in json_list:              
                    if 'device_ssen_temp_000' in item.keys():
                        print('Device SSen Temp: ',item['device_ssen_temp_000'],',',end = "")
           
           if adcs_print[8] == 1:
               for item in json_list: 
                    if 'device_ssen_azimuth_000' in item.keys():
                        print('Device SSEN Azimuth: ',item['device_ssen_azimuth_000'],',',end = "")
                        
                
           if adcs_print[9] == 1:
               for item in json_list:  
                    if 'device_ssen_elevation_000' in item.keys():
                        print('Device SSEN Elevation: ',item['device_ssen_elevation_000'],',',end = "")
                        
           if adcs_print[10] == 1:
               for item in json_list: 
                    if 'device_ssen_qva_000' in item.keys():
                        print('Device SSEN: \n QVA: ',item['device_ssen_qva_000'],',',end = "")
                        
           if adcs_print[11] == 1:
               for item in json_list: 
                    if 'device_ssen_qvb_000' in item.keys():
                        print(' QVB: ',item['device_ssen_qvb_000'],',',end = "")
                        
           if adcs_print[12] == 1:
               for item in json_list:         
                    if 'device_ssen_qvc_000' in item.keys():
                        print(' QVC: ',item['device_ssen_qvc_000'],',',end = "")
                
           if adcs_print[13] == 1:
               for item in json_list: 
                    if 'device_ssen_qvd_000' in item.keys():               
                        print(' QVD: ',item['device_ssen_qvd_000'],',',end = "")
                        
           if adcs_print[14] == 1:
               for item in json_list:  
                    if 'device_imu_utc_000' in item.keys():
                        print('Device IMU: \n UTC: ',item['device_imu_utc_000'],',',end = "")
                              
           if adcs_print[15] == 1:
               for item in json_list: 
                    if 'device_imu_temp_000' in item.keys():
                        print(' Temp: ',item['device_imu_temp_000'],',',end = "")
                            
           if adcs_print[16] == 1:
               for item in json_list: 
                    if 'device_imu_accel_000' in item.keys():         
                        print(' Accel: ',item['device_imu_accel_000'],',',end = "")
                            
           if adcs_print[17] == 1:
               for item in json_list: 
                    if 'device_imu_omega_000' in item.keys():         
                        print(round(item['device_imu_omega_000'][0]*(180/3.14159),3), ',',  \
                              round(item['device_imu_omega_000'][1]*(180/3.14159),3), ',' , \
                              round(item['device_imu_omega_000'][2]*(180/3.14159),3), ', ',end = "")
#                        newline = newline + str(round(item['device_imu_omega_000'][0]*(180/3.14159),3)) + ','
#                        newline = newline + str(round(item['device_imu_omega_000'][1]*(180/3.14159),3)) + ','
#                        newline = newline + str(round(item['device_imu_omega_000'][2]*(180/3.14159),3)) + ','
           if adcs_print[18] == 1:
               for item in json_list: 
                    if 'device_imu_alpha_000' in item.keys():         
                        print(round(item['device_imu_alpha_000'][0]*(180/3.14159),3), ',',  \
                              round(item['device_imu_alpha_000'][1]*(180/3.14159),3), ',' , \
                              round(item['device_imu_alpha_000'][2]*(180/3.14159),3), ', ',end = "")
                         
           if adcs_print[19] == 1:
               for item in json_list: 
                    if 'device_imu_mag_000' in item.keys():         
                        print(round(item['device_imu_mag_000'][0],3), ',',  \
                              round(item['device_imu_mag_000'][1],3), ',' , \
                              round(item['device_imu_mag_000'][2],3), ', ',end = "")
                         
           if adcs_print[20] == 1:
               for item in json_list: 
                    if 'device_imu_bdot_000' in item.keys():
                        print(round(item['device_imu_bdot_000'][0],3), ',',  \
                              round(item['device_imu_bdot_000'][1],3), ',' , \
                              round(item['device_imu_bdot_000'][2],3), ', ',end = "")
                        
           if adcs_print[21] == 1:
               for item in json_list: 
                    if 'device_rw_utc_000' in item.keys():
                        print('Device RW UTC: ',item['device_rw_utc_000'],',',end = "")
                        
           print('\n')
               
    
                            