#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 15:03:58 2017

@author: devantormey
"""
def print_eps_meta(agent_eps,json_list):  
    if agent_eps == 1:
        if json_list[2]['agent_proc'] == 'eps':
            if 'agent_utc' in json_list[0].keys():
                    print('\n \nAgent Type: ',json_list[2]['agent_proc'])
                    print('agent_utc (time): ', json_list[0]['agent_utc'])        
                    print('agent_node: ', json_list[1]['agent_node'])
                    print('cpu: ', json_list[6]['agent_cpu'], ' Memory: ', \
                          json_list[7]['agent_memory'], ' Jitter: ', \
                          json_list[8]['agent_jitter'], ' UTC Offset: ', json_list[9]['node_utcoffset'], '\n')
        

def print_exec(agent_exec,json_list,exec_print):  
   if agent_exec == 1: 
       if json_list[2]['agent_proc'] == 'exec':
           if 'agent_utc' in json_list[0].keys():
                   print('\n \nAgent Type: ',json_list[2]['agent_proc'])
                   print('agent_utc (time): ', json_list[0]['agent_utc'])        
                   print('agent_node: ', json_list[1]['agent_node'])
                   print('cpu: ', json_list[6]['agent_cpu'], ' Memory: ', \
                         json_list[7]['agent_memory'], ' Jitter: ', \
                         json_list[8]['agent_jitter'], ' UTC Offset: ', json_list[9]['node_utcoffset'], '\n')
           #Battery Print 
           if exec_print[2] == 1:        
                for item in json_list:
                    if 'node_powgen' in item.keys():
                        print('Power Gen: ',item['node_powgen'])
                                    
                    if 'node_powuse' in item.keys():
                        print('Power Use: ',item['node_powuse'])
                                    
                    if 'node_charging' in item.keys():
                        print('Charging: ',item['node_charging'])
                                    
                    if 'node_battlev' in item.keys():
                        print('Batt Level: ',item['node_battlev'])
           #location below earth
           if exec_print[3] == 1:
               for item in json_list:
                   if 'node_loc_bearth' in item.keys():
                       print('Location Bearth: ',item['node_loc_bearth'])
    
           #node Loc Pos
           if exec_print[4] == 1:
               for item in json_list:
                   if 'node_loc_pos_eci' in item.keys():
                       print('Loc Pos ECI: ', ' Pos: ' ,item['node_loc_pos_eci']['pos'], \
                             ' Velocity: ' ,item['node_loc_pos_eci']['vel'], \
                             ' Acceleration: ' ,item['node_loc_pos_eci']['acc'])
                       
           if exec_print[5] == 1:
               for item in json_list:           
                   if 'node_loc_att_icrf' in item.keys():
                       print('Loc Att ICRF: ', ' Pos: ' ,'[',item['node_loc_att_icrf']['pos']['d']['x'], \
                              item['node_loc_att_icrf']['pos']['d']['y'], \
                              item['node_loc_att_icrf']['pos']['d']['z'], \
                              item['node_loc_att_icrf']['pos']['w'],']' , \
                              ' Velocity: ' ,item['node_loc_att_icrf']['vel'], \
                              ' Acceleration: ' ,item['node_loc_att_icrf']['acc'])
                       
           if exec_print[6] == 1:
               for item in json_list:           
                    if 'device_ssen_utc_000' in item.keys():
                         print('Device SSen UTC: ',item['device_ssen_utc_000']) 
                         
           if exec_print[7] == 1:
               for item in json_list:              
                    if 'device_ssen_temp_000' in item.keys():
                        print('Device SSen Temp: ',item['device_ssen_temp_000'])
           
           if exec_print[8] == 1:
               for item in json_list: 
                    if 'device_ssen_azimuth_000' in item.keys():
                        print('Device SSEN Azimuth: ',item['device_ssen_azimuth_000'])
                        
                
           if exec_print[9] == 1:
               for item in json_list:  
                    if 'device_ssen_elevation_000' in item.keys():
                        print('Device SSEN Elevation: ',item['device_ssen_elevation_000'])
                        
           if exec_print[10] == 1:
               for item in json_list: 
                    if 'device_ssen_qva_000' in item.keys():
                        print('Device SSEN: \n QVA: ',item['device_ssen_qva_000'])
                        
           if exec_print[11] == 1:
               for item in json_list: 
                    if 'device_ssen_qvb_000' in item.keys():
                        print(' QVB: ',item['device_ssen_qvb_000'])
                        
           if exec_print[12] == 1:
               for item in json_list:         
                    if 'device_ssen_qvc_000' in item.keys():
                        print(' QVC: ',item['device_ssen_qvc_000'])
                
           if exec_print[13] == 1:
               for item in json_list: 
                    if 'device_ssen_qvd_000' in item.keys():               
                        print(' QVD: ',item['device_ssen_qvd_000'])
                        
           if exec_print[14] == 1:
               for item in json_list:  
                    if 'device_imu_utc_000' in item.keys():
                        print('Device IMU: \n UTC: ',item['device_imu_utc_000'])
                              
           if exec_print[15] == 1:
               for item in json_list: 
                    if 'device_imu_temp_000' in item.keys():
                        print(' Temp: ',item['device_imu_temp_000'])
                            
           if exec_print[16] == 1:
               for item in json_list: 
                    if 'device_imu_accel_000' in item.keys():         
                        print(' Accel: ',item['device_imu_accel_000'])
                            
           if exec_print[17] == 1:
               for item in json_list: 
                    if 'device_imu_omega_000' in item.keys():         
                        print(' Omega: ',item['device_imu_omega_000'])
                          
           if exec_print[18] == 1:
               for item in json_list: 
                    if 'device_imu_alpha_000' in item.keys():         
                        print(' Alpha: ',item['device_imu_alpha_000'])
                         
           if exec_print[19] == 1:
               for item in json_list: 
                    if 'device_imu_mag_000' in item.keys():         
                        print(' Mag: ',item['device_imu_mag_000'])
                         
           if exec_print[20] == 1:
               for item in json_list: 
                    if 'device_imu_bdot_000' in item.keys():        
                        print(' B-dot: ',item['device_imu_bdot_000'])
                        
           if exec_print[21] == 1:
               for item in json_list: 
                    if 'device_rw_utc_000' in item.keys():
                        print('Device RW UTC: ',item['device_rw_utc_000'], '\n')
               
    
                            