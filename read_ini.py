#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 13:45:03 2017

@author: devantormey
"""


def read_ini(filename):

    words = []
    names = []
    
    #Eps print
    
    #printing vector
    print_vec = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    count = 0
    even = 0
    
    with open(filename,'r') as f:
        for line in f:
            for word in line.split():
                words.append(word)
                if even%2 != 0:
                    print_vec[count] = int(word)
                    count = count + 1
                else: 
                    names.append(word)
                even = even + 1
#    close(filename)
    for k in range(1,len(print_vec)):
        if print_vec[k] == 1:
            print(names[k] + ',', end="")
    print('\n')
    return print_vec
        
        	