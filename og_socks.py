#AUTHOR: DEVAN TORMEY
#%%

from read_ini import *
from print_adcs import *


right_count = 0
left_count = 0 
end_flag = 0
end_data = 0
start_flag = 0
j = 0
k = 0
objects = []
count = 0
json_list = []
start = 0
omega_y = []
thefile = open('omega_y.csv', 'w')
#Place Holder for Ini File

#printing vector
#print_vec = read_ini('print.txt')
adcs_vec = read_ini('adcs.txt')

#agent_eps = print_vec[0]
#exec stuff
#agent_exec = print_vec[1] #basic print of name etc

from socket import *

import time

import json

from Agent_print import *

cs = socket(AF_INET, SOCK_DGRAM) #Create the 'Socket' (listening port) on this computer
try:
    cs.bind(('192.168.150.255', 10020)) #attatch the socket to the HSFL wifi IP
except:
    print('failed to bind') #fail case that never gets triggered
    cs.close()
    raise
    cs.blocking(0)
while 1: 
    data = cs.recvfrom(1024) #get data from port
    json_byte = data[0] # the data comes as a tuple (data,where it came from)
    json_str = data[0][3:-1] #this pulls only the JSON stuff and not the included info on size etc
    new_data = json_str.decode("utf-8") #it came in as type 'bit' this converts to str
    
    
#    parsed_json = json.loads(new_data) #this section was from testing (you can remove)
#    
#    print('DATA: \n')
#    print(data)
#    print('\n')
#    print('DATA AS STRING: \n')
#    print(new_data)
#    print('\n')
    
#~~~~~~~~~~~~~~~~~ Pullin apart the JSON objects from the string ~~~
    right_count = 0
    left_count = 0
    start_flag = 0
    start = 0
    for k in range(0, len(new_data)):
    
        if (new_data[k] == '{') & (start_flag == 1):
             left_count = left_count + 1
#             print('found that left boi')
         
        if (new_data[k] == '{') & (start_flag == 0):
            start = k
            start_flag = 1
            left_count = left_count + 1
#            print('begin')
            
        if (new_data[k] == '}'):
            right_count = right_count + 1
#            print('found that right boi')
            
        if (left_count == right_count) & (start_flag == 1):
            j_str = new_data[start:k+1]
            objects.append(j_str)
            start_flag = 0
            left_count = 0
            right_count = 0
#            print('we got a live one')
 

#~~~~~~~~~`JSON PARSER ~~~~~~~~~~~~
    for item in objects: #This takes that list of objects (each item being a string) and converts each one to a python objects           
        p_json = json.loads(item)
        json_list.append(p_json)
        
#~~~~~~~~~~~~~~~~this prints the list of the seperate json objects (before being parsed) for comparison
    print('List of Objects: \n')
    print(objects)
    print('\n')
    


#~~~~~~~~~~~~~~~~~~~~~~~ PRint statements for each agent this part is messy AF so have fun ~~~~~~~~
#    print_eps_meta(agent_eps,json_list) 
#    print_exec(agent_exec,json_list,print_vec)
    print_adcs(json_list,adcs_vec)
    

# MOITION TRACKER TESTING STUFF
#    if json_list[2]['agent_proc'] == 'motion':
#        for item in json_list:           
#            if 'node_loc_att_icrf' in item.keys():
#                thefile.write(str(item['node_loc_att_icrf']['utc']))
#                thefile.write(',')
#                thefile.write(str(item['node_loc_att_icrf']['vel'][0]))
#                thefile.write(',')
#                thefile.write(str(item['node_loc_att_icrf']['vel'][1]))
#                thefile.write(',')
#                thefile.write(str(item['node_loc_att_icrf']['vel'][2]))
#                thefile.write(',')
#                thefile.write('\n')
#                thefile.flush()
#                print(item['node_loc_att_icrf']['utc'],',',end = "")
#                print(item['node_loc_att_icrf']['vel'][0],',',end = "")
#                print(item['node_loc_att_icrf']['vel'][1],',',end = "")
#                print(item['node_loc_att_icrf']['vel'][2],',',end = "")
#                print('\n',end = "")
                

    objects = []
    json_list = []
    
    time.sleep(1)
      